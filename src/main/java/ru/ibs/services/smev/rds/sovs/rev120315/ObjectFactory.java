//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.10.16 at 11:59:31 PM MSK 
//


package ru.ibs.services.smev.rds.sovs.rev120315;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import ru.gosuslugi.smev.rev120315.SovsRequestType;
import ru.gosuslugi.smev.rev120315.SovsResponseType;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.ibs.services.smev.rds.sovs.rev120315 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Register_QNAME = new QName("http://ibs.ru/services/smev/rds/sovs/rev120315", "Register");
    private final static QName _RegisterResult_QNAME = new QName("http://ibs.ru/services/smev/rds/sovs/rev120315", "RegisterResult");
    private final static QName _Check_QNAME = new QName("http://ibs.ru/services/smev/rds/sovs/rev120315", "Check");
    private final static QName _CheckResult_QNAME = new QName("http://ibs.ru/services/smev/rds/sovs/rev120315", "CheckResult");
    private final static QName _Cancel_QNAME = new QName("http://ibs.ru/services/smev/rds/sovs/rev120315", "Cancel");
    private final static QName _CancelResult_QNAME = new QName("http://ibs.ru/services/smev/rds/sovs/rev120315", "CancelResult");
    private final static QName _SendFiles_QNAME = new QName("http://ibs.ru/services/smev/rds/sovs/rev120315", "SendFiles");
    private final static QName _SendFilesResult_QNAME = new QName("http://ibs.ru/services/smev/rds/sovs/rev120315", "SendFilesResult");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.ibs.services.smev.rds.sovs.rev120315
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SovsRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ibs.ru/services/smev/rds/sovs/rev120315", name = "Register")
    public JAXBElement<Register> createRegister(Register value) {
        return new JAXBElement<Register>(_Register_QNAME, Register.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SovsResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ibs.ru/services/smev/rds/sovs/rev120315", name = "RegisterResult")
    public JAXBElement<RegisterResult> createRegisterResult(RegisterResult value) {
        return new JAXBElement<RegisterResult>(_RegisterResult_QNAME, RegisterResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SovsRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ibs.ru/services/smev/rds/sovs/rev120315", name = "Check")
    public JAXBElement<Check> createCheck(Check value) {
        return new JAXBElement<Check>(_Check_QNAME, Check.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SovsResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ibs.ru/services/smev/rds/sovs/rev120315", name = "CheckResult")
    public JAXBElement<CheckResult> createCheckResult(CheckResult value) {
        return new JAXBElement<CheckResult>(_CheckResult_QNAME, CheckResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SovsRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ibs.ru/services/smev/rds/sovs/rev120315", name = "Cancel")
    public JAXBElement<Cancel> createCancel(Cancel value) {
        return new JAXBElement<Cancel>(_Cancel_QNAME, Cancel.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SovsResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ibs.ru/services/smev/rds/sovs/rev120315", name = "CancelResult")
    public JAXBElement<CancelResult> createCancelResult(CancelResult value) {
        return new JAXBElement<CancelResult>(_CancelResult_QNAME, CancelResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SovsRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ibs.ru/services/smev/rds/sovs/rev120315", name = "SendFiles")
    public JAXBElement<SendFiles> createSendFiles(SendFiles value) {
        return new JAXBElement<SendFiles>(_SendFiles_QNAME, SendFiles.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SovsResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ibs.ru/services/smev/rds/sovs/rev120315", name = "SendFilesResult")
    public JAXBElement<SendFilesResult> createSendFilesResult(SendFilesResult value) {
        return new JAXBElement<SendFilesResult>(_SendFilesResult_QNAME, SendFilesResult.class, null, value);
    }

}
