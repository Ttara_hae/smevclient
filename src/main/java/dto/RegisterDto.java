package dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import ru.gosuslugi.smev.rev120315.MessageFieldType;
import ru.ibs.services.smev.rds.sovs.rev120315.Register;

public class RegisterDto extends BaseDto {
    private String authorityName;
    private String outputNumber;
    private String outputDate;
    private String requestedRoute;
    private String transporterInfo;
    private String periodFrom;
    private String periodTo;
    private Integer tripCount;
    private String shippingType;
    private String freightName;
    private String freightDimension;
    private Double freightWeight;
    private String truckInfo;
    private String trailerInfo;
    private String emptyTruckWeight;
    private String emptyTrailerWeight;
    private String axlesInterval;
    private String axlesLoad;
    private String axlesCount;
    private Double totalWeight;
    private Double totalLength;
    private Double totalWidth;
    private Double totalHeight;
    private Double turnRadius;
    private String escortInfo;
    private Integer estimatedSpeed;

    public RegisterDto() {

    }

    public RegisterDto(Register register) {
        resolve(() -> register.getMessageData().getAppData().getXmlData().getRoot().getData().getField())
                .ifPresent(props -> props.forEach(prop -> fillProperty(prop)));
    }

    @Override
    protected void fillProperty(MessageFieldType messageFieldType) {
        switch (messageFieldType.getName()) {
            case "ApprovalID": this.approvalID = messageFieldType.getValue(); break;
            case "AuthorityName": this.authorityName = messageFieldType.getValue(); break;
            case "OutputNumber": this.outputNumber = messageFieldType.getValue(); break;
            case "OutputDate": this.outputDate = messageFieldType.getValue(); break;
            case "RequestedRoute": this.requestedRoute = messageFieldType.getValue(); break;
            case "TransporterInfo": this.transporterInfo = messageFieldType.getValue(); break;
            case "PeriodFrom": this.periodFrom = messageFieldType.getValue(); break;
            case "PeriodTo": this.periodTo = messageFieldType.getValue(); break;
            case "TripCount": this.tripCount = Integer.parseInt(messageFieldType.getValue()); break;
            case "ShippingType": this.shippingType = messageFieldType.getValue(); break;
            case "FreightName": this.freightName = messageFieldType.getValue(); break;
            case "FreightDimension": this.freightDimension = messageFieldType.getValue(); break;
            case "FreightWeight": this.freightWeight = Double.parseDouble(messageFieldType.getValue()); break;
            case "TruckInfo": this.truckInfo = messageFieldType.getValue(); break;
            case "TrailerInfo": this.trailerInfo = messageFieldType.getValue(); break;
            case "EmptyTruckWeight": this.emptyTruckWeight = messageFieldType.getValue(); break;
            case "EmptyTrailerWeight": this.emptyTrailerWeight = messageFieldType.getValue(); break;
            case "AxlesInterval": this.axlesInterval = messageFieldType.getValue(); break;
            case "AxlesLoad": this.axlesLoad = messageFieldType.getValue(); break;
            case "AxlesCount": this.axlesCount = messageFieldType.getValue(); break;
            case "TotalWeight": this.totalWeight = Double.parseDouble(messageFieldType.getValue()); break;
            case "TotalLength": this.totalLength = Double.parseDouble(messageFieldType.getValue()); break;
            case "TotalWidth": this.totalWidth = Double.parseDouble(messageFieldType.getValue()); break;
            case "TotalHeight": this.totalHeight = Double.parseDouble(messageFieldType.getValue()); break;
            case "TurnRadius": this.turnRadius = Double.parseDouble(messageFieldType.getValue()); break;
            case "EscortInfo": this.escortInfo = messageFieldType.getValue(); break;
            case "EstimatedSpeed": this.estimatedSpeed = Integer.parseInt(messageFieldType.getValue()); break;
        }
    }

    @JsonProperty("AuthorityName")
    public String getAuthorityName() {
        return authorityName;
    }

    public void setAuthorityName(String authorityName) {
        this.authorityName = authorityName;
    }

    @JsonProperty("OutputNumber")
    public String getOutputNumber() {
        return outputNumber;
    }

    public void setOutputNumber(String outputNumber) {
        this.outputNumber = outputNumber;
    }

    @JsonProperty("OutputDate")
    public String getOutputDate() {
        return outputDate;
    }

    public void setOutputDate(String outputDate) {
        this.outputDate = outputDate;
    }

    @JsonProperty("RequestedRoute")
    public String getRequestedRoute() {
        return requestedRoute;
    }

    public void setRequestedRoute(String requestedRoute) {
        this.requestedRoute = requestedRoute;
    }

    @JsonProperty("TransporterInfo")
    public String getTransporterInfo() {
        return transporterInfo;
    }

    public void setTransporterInfo(String transporterInfo) {
        this.transporterInfo = transporterInfo;
    }

    @JsonProperty("PeriodFrom")
    public String getPeriodFrom() {
        return periodFrom;
    }

    public void setPeriodFrom(String periodFrom) {
        this.periodFrom = periodFrom;
    }

    @JsonProperty("PeriodTo")
    public String getPeriodTo() {
        return periodTo;
    }

    public void setPeriodTo(String periodTo) {
        this.periodTo = periodTo;
    }

    @JsonProperty("TripCount")
    public Integer getTripCount() {
        return tripCount;
    }

    public void setTripCount(Integer tripCount) {
        this.tripCount = tripCount;
    }

    @JsonProperty("ShippingType")
    public String getShippingType() {
        return shippingType;
    }

    public void setShippingType(String shippingType) {
        this.shippingType = shippingType;
    }

    @JsonProperty("FreightName")
    public String getFreightName() {
        return freightName;
    }

    public void setFreightName(String freightName) {
        this.freightName = freightName;
    }

    @JsonProperty("FreightDimension")
    public String getFreightDimension() {
        return freightDimension;
    }

    public void setFreightDimension(String freightDimension) {
        this.freightDimension = freightDimension;
    }

    @JsonProperty("FreightWeight")
    public Double getFreightWeight() {
        return freightWeight;
    }

    public void setFreightWeight(Double freightWeight) {
        this.freightWeight = freightWeight;
    }

    @JsonProperty("TruckInfo")
    public String getTruckInfo() {
        return truckInfo;
    }

    public void setTruckInfo(String truckInfo) {
        this.truckInfo = truckInfo;
    }

    @JsonProperty("TrailerInfo")
    public String getTrailerInfo() {
        return trailerInfo;
    }

    public void setTrailerInfo(String trailerInfo) {
        this.trailerInfo = trailerInfo;
    }

    @JsonProperty("EmptyTruckWeight")
    public String getEmptyTruckWeight() {
        return emptyTruckWeight;
    }

    public void setEmptyTruckWeight(String emptyTruckWeight) {
        this.emptyTruckWeight = emptyTruckWeight;
    }

    @JsonProperty("EmptyTrailerWeight")
    public String getEmptyTrailerWeight() {
        return emptyTrailerWeight;
    }

    public void setEmptyTrailerWeight(String emptyTrailerWeight) {
        this.emptyTrailerWeight = emptyTrailerWeight;
    }

    @JsonProperty("AxlesInterval")
    public String getAxlesInterval() {
        return axlesInterval;
    }

    public void setAxlesInterval(String axlesInterval) {
        this.axlesInterval = axlesInterval;
    }

    @JsonProperty("AxlesLoad")
    public String getAxlesLoad() {
        return axlesLoad;
    }

    public void setAxlesLoad(String axlesLoad) {
        this.axlesLoad = axlesLoad;
    }

    @JsonProperty("AxlesCount")
    public String getAxlesCount() {
        return axlesCount;
    }

    public void setAxlesCount(String axlesCount) {
        this.axlesCount = axlesCount;
    }

    @JsonProperty("TotalWeight")
    public Double getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(Double totalWeight) {
        this.totalWeight = totalWeight;
    }

    @JsonProperty("TotalLength")
    public Double getTotalLength() {
        return totalLength;
    }

    public void setTotalLength(Double totalLength) {
        this.totalLength = totalLength;
    }

    @JsonProperty("TotalWidth")
    public Double getTotalWidth() {
        return totalWidth;
    }

    public void setTotalWidth(Double totalWidth) {
        this.totalWidth = totalWidth;
    }

    @JsonProperty("TotalHeight")
    public Double getTotalHeight() {
        return totalHeight;
    }

    public void setTotalHeight(Double totalHeight) {
        this.totalHeight = totalHeight;
    }

    @JsonProperty("TurnRadius")
    public Double getTurnRadius() {
        return turnRadius;
    }

    public void setTurnRadius(Double turnRadius) {
        this.turnRadius = turnRadius;
    }

    @JsonProperty("EscortInfo")
    public String getEscortInfo() {
        return escortInfo;
    }

    public void setEscortInfo(String escortInfo) {
        this.escortInfo = escortInfo;
    }

    @JsonProperty("EstimatedSpeed")
    public Integer getEstimatedSpeed() {
        return estimatedSpeed;
    }

    public void setEstimatedSpeed(Integer estimatedSpeed) {
        this.estimatedSpeed = estimatedSpeed;
    }

    @Override
    public String toString() {
        return "RegisterDto{" +
                "approvalID=" + approvalID +
                ", authorityName='" + authorityName + '\'' +
                ", outputNumber='" + outputNumber + '\'' +
                ", outputDate=" + outputDate +
                ", requestedRoute='" + requestedRoute + '\'' +
                ", transporterInfo='" + transporterInfo + '\'' +
                ", periodFrom=" + periodFrom +
                ", periodTo=" + periodTo +
                ", tripCount=" + tripCount +
                ", shippingType='" + shippingType + '\'' +
                ", freightName='" + freightName + '\'' +
                ", freightDimension='" + freightDimension + '\'' +
                ", freightWeight=" + freightWeight +
                ", truckInfo='" + truckInfo + '\'' +
                ", trailerInfo='" + trailerInfo + '\'' +
                ", emptyTruckWeight='" + emptyTruckWeight + '\'' +
                ", emptyTrailerWeight='" + emptyTrailerWeight + '\'' +
                ", axlesInterval='" + axlesInterval + '\'' +
                ", axlesLoad='" + axlesLoad + '\'' +
                ", axlesCount='" + axlesCount + '\'' +
                ", totalWeight=" + totalWeight +
                ", totalLength=" + totalLength +
                ", totalWidth=" + totalWidth +
                ", totalHeight=" + totalHeight +
                ", turnRadius=" + turnRadius +
                ", escortInfo='" + escortInfo + '\'' +
                ", estimatedSpeed=" + estimatedSpeed +
                '}';
    }
}
