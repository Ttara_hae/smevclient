package dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CheckResultDto extends BaseResultDto {
    private String status;
    private String outputNumber;
    private String outputDate;
    private String approvedPeriodFrom;
    private String approvedPeriodTo;
    private String approvedRoute;
    private String specialConditions;
    private String personName;
    private String personJob;
    private String comment;

    public CheckResultDto() {
    }

    @JsonProperty("Status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("OutputNumber")
    public String getOutputNumber() {
        return outputNumber;
    }

    public void setOutputNumber(String outputNumber) {
        this.outputNumber = outputNumber;
    }

    @JsonProperty("OutputDate")
    public String getOutputDate() {
        return outputDate;
    }

    public void setOutputDate(String outputDate) {
        this.outputDate = outputDate;
    }

    @JsonProperty("ApprovedPeriodFrom")
    public String getApprovedPeriodFrom() {
        return approvedPeriodFrom;
    }

    public void setApprovedPeriodFrom(String approvedPeriodFrom) {
        this.approvedPeriodFrom = approvedPeriodFrom;
    }

    @JsonProperty("ApprovedPeriodTo")
    public String getApprovedPeriodTo() {
        return approvedPeriodTo;
    }

    public void setApprovedPeriodTo(String approvedPeriodTo) {
        this.approvedPeriodTo = approvedPeriodTo;
    }

    @JsonProperty("ApprovedRoute")
    public String getApprovedRoute() {
        return approvedRoute;
    }

    public void setApprovedRoute(String approvedRoute) {
        this.approvedRoute = approvedRoute;
    }

    @JsonProperty("SpecialConditions")
    public String getSpecialConditions() {
        return specialConditions;
    }

    public void setSpecialConditions(String specialConditions) {
        this.specialConditions = specialConditions;
    }

    @JsonProperty("PersonName")
    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    @JsonProperty("PersonJob")
    public String getPersonJob() {
        return personJob;
    }

    public void setPersonJob(String personJob) {
        this.personJob = personJob;
    }

    @JsonProperty("Comment")
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "CheckResultDto{" +
                "status='" + status + '\'' +
                ", outputNumber='" + outputNumber + '\'' +
                ", outputDate='" + outputDate + '\'' +
                ", approvedPeriodFrom='" + approvedPeriodFrom + '\'' +
                ", approvedPeriodTo='" + approvedPeriodTo + '\'' +
                ", approvedRoute='" + approvedRoute + '\'' +
                ", specialConditions='" + specialConditions + '\'' +
                ", personName='" + personName + '\'' +
                ", personJob='" + personJob + '\'' +
                ", comment='" + comment + '\'' +
                ", value='" + value + '\'' +
                ", errorCode='" + errorCode + '\'' +
                ", timestamp='" + timestamp + '\'' +
                '}';
    }
}
