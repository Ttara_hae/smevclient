package dto;

import ru.gosuslugi.smev.rev120315.AppDocumentType;
import ru.ibs.services.smev.rds.sovs.rev120315.SendFiles;

public class SendFilesDto extends BaseDto {
    private String requestCode;
    private byte[] binaryData;

    public SendFilesDto() {

    }

    public SendFilesDto(SendFiles request) {
        super(request.getMessageData());
        resolve(() -> request.getMessageData().getAppDocument()).ifPresent(appDocument -> fillData(appDocument));
    }

    private void fillData(AppDocumentType appDocumentType) {
        this.requestCode = appDocumentType.getRequestCode();
        this.binaryData = appDocumentType.getBinaryData();
    }

    public String getRequestCode() {
        return requestCode;
    }

    public void setRequestCode(String requestCode) {
        this.requestCode = requestCode;
    }

    public byte[] getBinaryData() {
        return binaryData;
    }

    public void setBinaryData(byte[] binaryData) {
        this.binaryData = binaryData;
    }

    @Override
    public String toString() {
        return "SendFilesDto{" +
                "requestCode='" + requestCode + '\'' +
                ", approvalID='" + approvalID + '\'' +
                '}';
    }
}
