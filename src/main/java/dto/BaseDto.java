package dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import ru.gosuslugi.smev.rev120315.MessageFieldType;
import ru.gosuslugi.smev.rev120315.SovsMessageDataType;

import java.util.Optional;
import java.util.function.Supplier;

public class BaseDto {
    protected String approvalID;

    public BaseDto() {
    }

    public BaseDto(SovsMessageDataType messageDataType) {
        resolve(() -> messageDataType.getAppData().getXmlData().getRoot().getData().getField())
                .ifPresent(props -> props.forEach(prop -> fillProperty(prop)));
    }

    @JsonProperty("ApprovalID")
    public String getApprovalID() {
        return approvalID;
    }

    public void setApprovalID(String approvalID) {
        this.approvalID = approvalID;
    }

    public <T> Optional<T> resolve(Supplier<T> resolver) {
        try {
            T result = resolver.get();
            return Optional.ofNullable(result);
        }
        catch (NullPointerException e) {
            return Optional.empty();
        }
    }

    protected void fillProperty(MessageFieldType messageFieldType) {
        if (messageFieldType != null && "ApprovalID".equals(messageFieldType.getName())) {
            this.approvalID = messageFieldType.getValue();
        }
    }

    @Override
    public String toString() {
        return "BaseDto{" +
                "approvalID='" + approvalID + '\'' +
                '}';
    }
}
