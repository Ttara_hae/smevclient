package smev;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.security.cert.Certificate;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

@Component
public class SmevSign {
    private static Logger logger = LoggerFactory.getLogger(SmevSign.class);

    private PrivateKey privateKey;
    private Certificate x509Cert;

    public SmevSign(@Value("${keystore.alias}") String keyStoreAlias, @Value("${keystore.password}") String keyStorePassword) {
        try {
            KeyStore ks = KeyStore.getInstance("HDImageStore", "JCP");
            ks.load(null, keyStorePassword.toCharArray());
            privateKey = (PrivateKey) ks.getKey(keyStoreAlias, keyStorePassword.toCharArray());
            x509Cert = ks.getCertificate(keyStoreAlias);

            ru.CryptoPro.JCPxml.XmlInit.init();
        } catch (KeyStoreException | NoSuchProviderException | UnrecoverableKeyException | NoSuchAlgorithmException |
                CertificateException | IOException e) {
            logger.error("Error during crypto pro init", e);
        }
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(PrivateKey privateKey) {
        this.privateKey = privateKey;
    }

    public Certificate getX509Cert() {
        return x509Cert;
    }

    public void setX509Cert(Certificate x509Cert) {
        this.x509Cert = x509Cert;
    }
}
