package app;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.client.RestTemplate;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;
import org.w3._2000._09.xmldsig_.SignatureType;
import org.w3._2004._08.xop.include.Include;
import ru.ibs.services.smev.rds.sovs.rev120315.Cancel;
import ru.ibs.services.smev.rds.sovs.rev120315.CancelResult;
import ru.ibs.services.smev.rds.sovs.rev120315.Check;
import ru.ibs.services.smev.rds.sovs.rev120315.CheckResult;
import ru.ibs.services.smev.rds.sovs.rev120315.Register;
import ru.gosuslugi.smev.rev120315.SovsRequestType;
import ru.gosuslugi.smev.rev120315.SovsResponseType;
import ru.ibs.services.smev.rds.sovs.rev120315.RegisterResult;
import ru.ibs.services.smev.rds.sovs.rev120315.SendFiles;
import ru.ibs.services.smev.rds.sovs.rev120315.SendFilesResult;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

@EnableWs
@Configuration
@ComponentScan({"smev"})
public class WebServiceConfig {
    @Bean
    public ServletRegistrationBean messageDispatcherServlet(ApplicationContext applicationContext) {
        MessageDispatcherServlet servlet = new MessageDispatcherServlet();
        servlet.setApplicationContext(applicationContext);
        servlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean(servlet, "/ws/*");
    }

    @Bean(name = "smev")
    public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema smevSchema) {
        DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName("smev");
        wsdl11Definition.setLocationUri("/ws");
        wsdl11Definition.setTargetNamespace("http://ibs.ru/services/smev/rds/sovs/rev120315");
        wsdl11Definition.setSchema(smevSchema);
        return wsdl11Definition;
    }

    @Bean
    public XsdSchema smevSchema() {
        return new SimpleXsdSchema(new ClassPathResource("request.xsd"));
    }

    @Bean
    public JAXBContext jaxbContext() throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(Register.class, RegisterResult.class, SovsRequestType.class, SovsResponseType.class, Include.class, SignatureType.class,
                SendFiles.class, SendFilesResult.class, Check.class, CheckResult.class, Cancel.class, CancelResult.class);
        System.setProperty("javax.xml.bind.JAXBContext", "app.JaxbControl");
        return jaxbContext;
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @Bean
    public CustomEndpointInterceptor customSmartEndpointInterceptor() {
        return new CustomEndpointInterceptor();
    }
}
