package app;

import dto.BaseDto;
import dto.BaseResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class RestClient {
    @Autowired
    private RestTemplate restTemplate;

    public <T extends BaseResultDto> T sendRequest(BaseDto dto, String url, Class<T> clazz) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<BaseDto> httpEntity = new HttpEntity<>(dto, headers);
        return restTemplate.postForObject(url, httpEntity, clazz);
    }
}
