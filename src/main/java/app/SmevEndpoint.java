package app;

import dto.BaseDto;
import dto.BaseResultDto;
import dto.CheckResultDto;
import dto.RegisterDto;
import dto.SendFilesDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.RestClientException;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.gosuslugi.smev.rev120315.SovsMessageDataType;
import ru.ibs.services.smev.rds.sovs.rev120315.Cancel;
import ru.ibs.services.smev.rds.sovs.rev120315.CancelResult;
import ru.ibs.services.smev.rds.sovs.rev120315.Check;
import ru.ibs.services.smev.rds.sovs.rev120315.CheckResult;
import ru.ibs.services.smev.rds.sovs.rev120315.Register;
import ru.ibs.services.smev.rds.sovs.rev120315.RegisterResult;
import ru.ibs.services.smev.rds.sovs.rev120315.SendFiles;
import ru.ibs.services.smev.rds.sovs.rev120315.SendFilesResult;

import javax.xml.bind.JAXBContext;
import javax.xml.datatype.DatatypeConfigurationException;
import java.util.UUID;

@Endpoint
public class SmevEndpoint {
    private static Logger logger = LoggerFactory.getLogger(SmevEndpoint.class);
    private static final String NAMESPACE_URI = "http://ibs.ru/services/smev/rds/sovs/rev120315";

    @Autowired
    private RestClient restClient;
    @Autowired
    private JAXBContext jaxbContext;
    @Value("${register.url}")
    public String registerUrl;
    @Value("${send.files.url}")
    public String sendFilesUrl;
    @Value("${check.url}")
    public String checkUrl;
    @Value("${cancel.url}")
    public String cancelUrl;


    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "Register")
    @ResponsePayload
    public RegisterResult register(@RequestPayload Register registerRequest) {
        RegisterResult response = new RegisterResult();
        response.setMessage(registerRequest.getMessage());
        response.getMessage().setRequestIdRef(UUID.randomUUID().toString());
        try {
            BaseResultDto resultDto = new BaseResultDto();
            try {
                resultDto = restClient.sendRequest(new RegisterDto(registerRequest), registerUrl, BaseResultDto.class);
                SovsMessageDataType messageDataType = DtoUtils.initSovsMessageDataType(resultDto);
                response.setMessageData(messageDataType);
            } catch (RestClientException e) {
                logger.error("Error during register request", e);
                SovsMessageDataType messageDataType = DtoUtils.initSovsMessageDataType(app.Value.ERROR.name(), String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
                response.setMessageData(messageDataType);
            }
        } catch (DatatypeConfigurationException e) {
            logger.error("Error during creating response", e);
        }
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "SendFiles")
    @ResponsePayload
    public SendFilesResult sendFiles(@RequestPayload SendFiles sendFilesRequest) {
        SendFilesResult response = new SendFilesResult();
        response.setMessage(sendFilesRequest.getMessage());
        response.getMessage().setRequestIdRef(UUID.randomUUID().toString());
        try {
            BaseResultDto resultDto = new BaseResultDto();
            try {
                resultDto = restClient.sendRequest(new SendFilesDto(sendFilesRequest), sendFilesUrl, BaseResultDto.class);
                SovsMessageDataType messageDataType = DtoUtils.initSovsMessageDataType(resultDto);
                response.setMessageData(messageDataType);
            } catch (RestClientException e) {
                logger.error("Error during sendFiles request", e);
                SovsMessageDataType messageDataType = DtoUtils.initSovsMessageDataType(app.Value.ERROR.name(), String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
                response.setMessageData(messageDataType);
            }
        } catch (DatatypeConfigurationException e) {
            logger.error("Error during register request", e);
        }
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "Check")
    @ResponsePayload
    public CheckResult check(@RequestPayload Check checkRequest) {
        CheckResult response = new CheckResult();
        response.setMessage(checkRequest.getMessage());
        response.getMessage().setRequestIdRef(UUID.randomUUID().toString());
        try {
            CheckResultDto resultDto = new CheckResultDto();
            try {
                resultDto = restClient.sendRequest(new BaseDto(checkRequest.getMessageData()), checkUrl, CheckResultDto.class);
                SovsMessageDataType messageDataType = DtoUtils.initSovsMessageDataType(resultDto);
                DtoUtils.fillCheckMessage(messageDataType, resultDto);
                response.setMessageData(messageDataType);
            } catch (RestClientException e) {
                logger.error("Error during check request", e);
                SovsMessageDataType messageDataType = DtoUtils.initSovsMessageDataType(app.Value.ERROR.name(), String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
                response.setMessageData(messageDataType);
            }
        } catch (DatatypeConfigurationException e) {
            logger.error("Error during register request", e);
        }
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "Cancel")
    @ResponsePayload
    public CancelResult cancel(@RequestPayload Cancel cancelRequest) {
        CancelResult response = new CancelResult();
        response.setMessage(cancelRequest.getMessage());
        response.getMessage().setRequestIdRef(UUID.randomUUID().toString());
        try {
            BaseResultDto resultDto = new BaseResultDto();
            try {
                resultDto = restClient.sendRequest(new BaseDto(cancelRequest.getMessageData()), cancelUrl, BaseResultDto.class);
                SovsMessageDataType messageDataType = DtoUtils.initSovsMessageDataType(resultDto);
                response.setMessageData(messageDataType);
            } catch (RestClientException e) {
                logger.error("Error during cancel request", e);
                SovsMessageDataType messageDataType = DtoUtils.initSovsMessageDataType(app.Value.ERROR.name(), String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
                response.setMessageData(messageDataType);
            }
        } catch (DatatypeConfigurationException e) {
            logger.error("Error during register request", e);
        }
        return response;
    }
}
