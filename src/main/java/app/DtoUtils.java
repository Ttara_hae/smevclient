package app;

import dto.BaseResultDto;
import dto.CheckResultDto;
import ru.gosuslugi.smev.rev120315.ArrayOfMessageFieldType;
import ru.gosuslugi.smev.rev120315.MessageFieldType;
import ru.gosuslugi.smev.rev120315.MessageResponseType;
import ru.gosuslugi.smev.rev120315.MessageRootType;
import ru.gosuslugi.smev.rev120315.SovsAppDataType;
import ru.gosuslugi.smev.rev120315.SovsMessageDataType;
import ru.gosuslugi.smev.rev120315.XmlDataType;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.time.Instant;
import java.util.List;

public class DtoUtils {
    /**
     * Create SovsMessageDataType with init inner objects
     * @param value - response value
     * @param errorCode - response errorCode
     * @return SovsMessageDataType
     * @throws DatatypeConfigurationException
     */
    public static SovsMessageDataType initSovsMessageDataType(String value, String errorCode) throws DatatypeConfigurationException {
        SovsMessageDataType messageDataType = new SovsMessageDataType();

//        appDataType
        SovsAppDataType appDataType = new SovsAppDataType();
        messageDataType.setAppData(appDataType);
        XmlDataType xmlDataType = new XmlDataType();
        appDataType.setXmlData(xmlDataType);

        MessageRootType messageRootType = new MessageRootType();
        xmlDataType.setRoot(messageRootType);

        MessageResponseType messageResponseType = new MessageResponseType();
        messageResponseType.setValue(value);
        messageResponseType.setErrorCode(errorCode);
        messageResponseType.setTimestamp(DatatypeFactory.newInstance().newXMLGregorianCalendar(Instant.now().toString()));
        messageRootType.setResponse(messageResponseType);

        ArrayOfMessageFieldType data = new ArrayOfMessageFieldType();
        messageRootType.setData(data);

        return messageDataType;
    }

    /**
     * Create SovsMessageDataType with init inner objects
     * @param resultDto - BaseResultDto(value, errorCode, timestamp)
     * @return SovsMessageDataType
     * @throws DatatypeConfigurationException
     */
    public static SovsMessageDataType initSovsMessageDataType(BaseResultDto resultDto) throws DatatypeConfigurationException {
        SovsMessageDataType messageDataType = new SovsMessageDataType();

//        appDataType
        SovsAppDataType appDataType = new SovsAppDataType();
        messageDataType.setAppData(appDataType);
        XmlDataType xmlDataType = new XmlDataType();
        appDataType.setXmlData(xmlDataType);

        MessageRootType messageRootType = new MessageRootType();
        xmlDataType.setRoot(messageRootType);

        MessageResponseType messageResponseType = new MessageResponseType();
        messageResponseType.setValue(resultDto.getValue());
        messageResponseType.setErrorCode(resultDto.getErrorCode());
        if (resultDto.getTimestamp() != null) {
            messageResponseType.setTimestamp(DatatypeFactory.newInstance().newXMLGregorianCalendar(resultDto.getTimestamp()));
        } else {
            messageResponseType.setTimestamp(DatatypeFactory.newInstance().newXMLGregorianCalendar(Instant.now().toString()));
        }
        messageRootType.setResponse(messageResponseType);

        ArrayOfMessageFieldType data = new ArrayOfMessageFieldType();
        messageRootType.setData(data);

        return messageDataType;
    }

    /**
     * Create message fields from checkResultDto
     * @param messageDataType
     * @param checkResultDto
     */
    public static void fillCheckMessage(SovsMessageDataType messageDataType, CheckResultDto checkResultDto) {
        List<MessageFieldType> fields = messageDataType.getAppData().getXmlData().getRoot().getData().getField();
//        Status
        MessageFieldType status = new MessageFieldType();
        status.setName("Status");
        status.setValue(checkResultDto.getStatus());
        fields.add(status);
//        OutputNumber
        MessageFieldType outputNumber = new MessageFieldType();
        outputNumber.setName("OutputNumber");
        outputNumber.setValue(checkResultDto.getOutputNumber());
        fields.add(outputNumber);
//        OutputDate
        MessageFieldType outputDate = new MessageFieldType();
        outputDate.setName("OutputDate");
        outputDate.setValue(checkResultDto.getOutputDate());
        fields.add(outputDate);
//        ApprovedPeriodFrom
        MessageFieldType approvedPeriodFrom = new MessageFieldType();
        approvedPeriodFrom.setName("ApprovedPeriodFrom");
        approvedPeriodFrom.setValue(checkResultDto.getApprovedPeriodFrom());
        fields.add(approvedPeriodFrom);
//        ApprovedPeriodTo
        MessageFieldType approvedPeriodTo = new MessageFieldType();
        approvedPeriodTo.setName("ApprovedPeriodTo");
        approvedPeriodTo.setValue(checkResultDto.getApprovedPeriodTo());
        fields.add(approvedPeriodTo);
//        ApprovedRoute
        MessageFieldType approvedRoute = new MessageFieldType();
        approvedRoute.setName("ApprovedRoute");
        approvedRoute.setValue(checkResultDto.getApprovedRoute());
        fields.add(approvedRoute);
//        SpecialConditions
        MessageFieldType specialConditions = new MessageFieldType();
        specialConditions.setName("SpecialConditions");
        specialConditions.setValue(checkResultDto.getSpecialConditions());
        fields.add(specialConditions);
//        PersonName
        MessageFieldType personName = new MessageFieldType();
        personName.setName("PersonName");
        personName.setValue(checkResultDto.getPersonName());
        fields.add(personName);
//        PersonJob
        MessageFieldType personJob = new MessageFieldType();
        personJob.setName("PersonJob");
        personJob.setValue(checkResultDto.getPersonJob());
        fields.add(personJob);
//        Comment
        MessageFieldType comment = new MessageFieldType();
        comment.setName("Comment");
        comment.setValue(checkResultDto.getComment());
        fields.add(comment);
    }
}
