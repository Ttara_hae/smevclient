package app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.soap.SoapHeaderElement;
import org.springframework.ws.soap.saaj.SaajSoapMessage;
import org.springframework.ws.soap.server.SmartSoapEndpointInterceptor;
import org.springframework.ws.transport.context.TransportContext;
import org.springframework.ws.transport.context.TransportContextHolder;
import org.springframework.ws.transport.http.HttpServletConnection;
import smev.SmevSign;

import javax.servlet.http.HttpServletRequest;
import java.security.cert.X509Certificate;

public class CustomEndpointInterceptor implements SmartSoapEndpointInterceptor {
    private static final Logger logger = LoggerFactory.getLogger(CustomEndpointInterceptor.class);
    @Autowired
    private SmevSign smevSign;

    @Override
    public boolean shouldIntercept(MessageContext messageContext, Object o) {
        return true;
    }

    @Override
    public boolean understands(SoapHeaderElement soapHeaderElement) {
        return true;
    }

    @Override
    public boolean handleRequest(MessageContext messageContext, Object o) throws Exception {
        logger.info("Received request from IP address " + getIpAddress());
        SaajSoapMessage soapRequest = (SaajSoapMessage) messageContext.getRequest();
        Boolean valid = WSSecurityUtils.validate(soapRequest.getSaajMessage());
        if (!valid) {
            logger.error("Invalid signature received");
            throw new Exception("Invalid signature");
        }
        return valid;
    }

    @Override
    public boolean handleResponse(MessageContext messageContext, Object o) throws Exception {
        SaajSoapMessage soapResponse = (SaajSoapMessage) messageContext.getResponse();
        WSSecurityUtils.sign(smevSign.getPrivateKey(), (X509Certificate) smevSign.getX509Cert(), soapResponse.getSaajMessage());
        return true;
    }

    @Override
    public boolean handleFault(MessageContext messageContext, Object o) throws Exception {
        return true;
    }

    @Override
    public void afterCompletion(MessageContext messageContext, Object o, Exception e) throws Exception {

    }

    private String getIpAddress() {
        TransportContext context = TransportContextHolder.getTransportContext();
        HttpServletConnection connection = (HttpServletConnection) context.getConnection();
        HttpServletRequest request = connection.getHttpServletRequest();
        return request.getRemoteAddr();
    }
}
