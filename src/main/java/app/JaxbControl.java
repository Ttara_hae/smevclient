package app;

import javax.xml.bind.JAXBContext;
import java.util.Map;

public class JaxbControl {
    public static JAXBContext createContext(Class[] classes, Map<String, Object> properties) {
        return SpringContext.getBean(JAXBContext.class);
    }
}
